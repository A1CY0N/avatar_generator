# Avatar-generator

## Description

Random avatar generator

## Installation
```sh
git clone https://gitlab.com/A1CY0N/avatar_generator.git
cd avatar_generator
firefox index.html
```
## Use

Create a personalised avatar in 3 clicks? Easy!

## Application overview
![Avatar generator](screenshot.png "Avatar generator")

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/avatar_generator.git>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request